#![cfg(test)]
#![cfg_attr(feature = "nightly", feature(generator_trait))]

use generate::{generator, iterate, iterate_with_return, Generator, GeneratorState};

fn exhaust<G, R, Y, Ret>(
    gen: G,
    mut resume: R,
    expected_calls: usize,
    mut cb_yield: impl FnMut(Y) -> R,
) -> Ret
where
    G: Generator<R, Yield = Y, Return = Ret>,
{
    let mut gen = Box::pin(gen);
    let mut calls = 0;

    loop {
        match gen.as_mut().resume(resume) {
            GeneratorState::Yielded(val) => {
                resume = cb_yield(val);
                calls += 1
            }
            GeneratorState::Complete(val) => {
                assert_eq!(calls, expected_calls);
                return val;
            }
        }
    }
}

#[test]
fn build_generator() {
    println!("start");
    let gen = generator! {
        println!("pre-yield");
        yield 1;
        println!("post-yield");
        return "foo"
    };

    let v = exhaust(gen, (), 1, |v| {
        assert_eq!(v, 1);
    });

    assert_eq!(v, "foo");
}

#[test]
fn infer_types() {
    let gen = generator! {
        yield;
    };

    let v = exhaust(gen, (), 1, |v| {
        assert_eq!(v, ());
    });

    assert_eq!(v, ());
}

#[test]
fn can_resume_with() {
    let gen = generator! { |mut val|
        while val != 0 {
            val = yield val * 2;
        }
        val
    };

    let mut numbers = vec![5, 7, 3, 8, 4];
    let mut last_num = numbers.pop().unwrap();

    let val = exhaust(gen, last_num, 5, |v| {
        assert_eq!(last_num * 2, v);

        if let Some(val) = numbers.pop() {
            last_num = val;
            val
        } else {
            0
        }
    });

    assert_eq!(val, 0);
}

#[test]
fn can_iterate() {
    let gen = generator! {
        let mut i = 1;

        while i < 10 {
            yield i;
            i += 1;
        }
    };

    let iter = iterate(gen);

    assert_eq!(iter.collect::<Vec<_>>(), vec![1, 2, 3, 4, 5, 6, 7, 8, 9])
}

#[test]
fn can_iterate_with_handle_return() {
    let gen = generator! {
        let mut i = 1;

        while i < 10 {
            yield i;
            i += 1;
        }

        "foo"
    };

    let iter = iterate_with_return(gen, |ret| {
        assert_eq!("foo", ret);
        None
    });

    assert_eq!(iter.collect::<Vec<_>>(), vec![1, 2, 3, 4, 5, 6, 7, 8, 9])
}
